package io.tobbit.identity.config

import io.tobbit.identity.service.auth.AuthenticationFilterBuilder
import io.tobbit.identity.service.auth.AuthenticationHandlerBuilder
import io.tobbit.identity.service.auth.AuthenticationProviderBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.authentication.logout.LogoutFilter
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
open class WebSecurityConfig(
  private val afb: AuthenticationFilterBuilder,
  private val apb: AuthenticationProviderBuilder,
  private val ahb: AuthenticationHandlerBuilder,
  private val userDetailsService: UserDetailsService
) : WebSecurityConfigurerAdapter(), WebMvcConfigurer {

  private val indexUrl = "/ui/index.html"
  private val loginUrl = "/ui/login.html"
  private val formLoginUrl = "/form-login"
  private val signupUrl = "/signup"
  private val signinUrl = "/signin"

  private val resourcePaths = arrayOf("/webjars/**", "/favicon.ico", "/ui/**")
  private val loginPaths = arrayOf(loginUrl, "$signinUrl/**", formLoginUrl, "/logout")
  private val oauthPaths = arrayOf("/oauth/authorize")
  private val registerPaths = arrayOf(signupUrl, "/user/register")

  private val publicApiPaths = arrayOf("/client/**", "/social/login-configs")

  @Bean
  override fun authenticationManager(): AuthenticationManager = super.authenticationManager()

  override fun userDetailsService(): UserDetailsService = userDetailsService

  override fun configure(http: HttpSecurity) {
    val loginEntryPoint = ahb.loginEntryPoint(loginUrl)
    val socialLoginFilter = afb.socialLoginFilter(authenticationManager(), signinUrl, "$loginUrl?error=social")
    val formLoginFilter = afb.formLoginFilter(authenticationManager(), formLoginUrl, "$loginUrl?error=form")
    val logoutFilter = afb.logoutFilter("$loginUrl?logout")
    http
      .csrf().disable()

      .authorizeRequests()
      .antMatchers(*resourcePaths, *loginPaths, *oauthPaths, *registerPaths, *publicApiPaths)
      .permitAll()

      .and()
      .authorizeRequests()
      .anyRequest()
      .authenticated()

      .and()
      .httpBasic()
      .authenticationEntryPoint(loginEntryPoint)

      .and()
      .exceptionHandling()
      .authenticationEntryPoint(loginEntryPoint)

      .and()
      .addFilterBefore(socialLoginFilter, AbstractPreAuthenticatedProcessingFilter::class.java)
      //.addFilterAt(OAuth2ClientAuthenticationProcessingFilter)
      .addFilterAt(formLoginFilter, UsernamePasswordAuthenticationFilter::class.java)
      .addFilterAt(logoutFilter, LogoutFilter::class.java)
  }

  override fun configure(auth: AuthenticationManagerBuilder) {
    auth
      .authenticationProvider(apb.formLoginAuthenticationProvider())
      .authenticationProvider(apb.socialAuthenticationProvider())
  }

  override fun addViewControllers(registry: ViewControllerRegistry) {
    // registry.addViewController(loginUrl).setViewName("login")
    // registry.addViewController(signupUrl).setViewName("signup")
    registry.addRedirectViewController("/", indexUrl)
    registry.addRedirectViewController("/index.html", indexUrl)
    registry.setOrder(Ordered.HIGHEST_PRECEDENCE)
  }

}
