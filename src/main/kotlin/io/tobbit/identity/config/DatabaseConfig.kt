package io.tobbit.identity.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@EnableJpaRepositories(basePackages = ["io.t0bst4r.identity.repository.jpa"])
@EntityScan(basePackages = ["io.tobbit.identity.model.jpa"])
open class DatabaseConfig
