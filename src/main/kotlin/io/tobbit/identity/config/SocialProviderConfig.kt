package io.tobbit.identity.config

import com.fasterxml.jackson.databind.ObjectMapper
import io.tobbit.identity.config.properties.SocialProperties
import io.tobbit.identity.service.SocialService
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.social.facebook.security.FacebookAuthenticationService
import org.springframework.social.security.provider.OAuth2AuthenticationService
import org.springframework.social.security.provider.SocialAuthenticationService

@Configuration
@EnableConfigurationProperties(SocialProperties::class)
open class SocialProviderConfig {

  private val redirectProperties = setOf("client_id", "next")

  @Bean("socialAuthenticationServices")
  open fun socialAuthenticationServices(
    socialService: SocialService,
    objectMapper: ObjectMapper
  ): List<SocialAuthenticationService<*>> = socialService.configuredServices
    .mapNotNull { toAuthenticationService(it.key, it.value) }
    .map { configureGlobals(it) }

  private fun toAuthenticationService(providerId: String, properties: SocialProperties.SocialAuthProperties) =
    when (providerId) {
      "facebook" -> facebookAuthenticationService(properties)
      else -> null
    }

  private fun configureGlobals(service: OAuth2AuthenticationService<*>) = service
    .also { it.returnToUrlParameters = (it.returnToUrlParameters ?: emptySet()) }
    .also { it.returnToUrlParameters = it.returnToUrlParameters.plus(redirectProperties) }

  private fun facebookAuthenticationService(properties: SocialProperties.SocialAuthProperties): OAuth2AuthenticationService<*> {
    return FacebookAuthenticationService(properties.clientId, properties.clientSecret)
      .also { it.setDefaultScope((properties.scopes ?: emptyList()).joinToString(",")) }
      .also { it.afterPropertiesSet() }
  }
}
