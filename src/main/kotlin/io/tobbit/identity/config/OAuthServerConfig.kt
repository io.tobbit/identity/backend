package io.tobbit.identity.config

import io.tobbit.identity.config.properties.AuthorizationServerProperties
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer
import org.springframework.security.oauth2.provider.ClientDetailsService
import org.springframework.security.oauth2.provider.endpoint.DefaultRedirectResolver
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore

@Configuration
@EnableAuthorizationServer
@EnableConfigurationProperties(AuthorizationServerProperties::class)
open class OAuthServerConfig(
  private val passwordEncoder: PasswordEncoder,
  private val authenticationManager: AuthenticationManager,
  private val authorizationServerProperties: AuthorizationServerProperties,
  private val clientDetailsService: ClientDetailsService
) : AuthorizationServerConfigurerAdapter() {

  override fun configure(security: AuthorizationServerSecurityConfigurer) {
    security
      .passwordEncoder(passwordEncoder)
      .tokenKeyAccess("permitAll()")
      .checkTokenAccess("isAuthenticated()")
  }

  override fun configure(clients: ClientDetailsServiceConfigurer) {
    clients.withClientDetails(clientDetailsService)
  }

  override fun configure(endpoints: AuthorizationServerEndpointsConfigurer) {
    endpoints
      .authenticationManager(authenticationManager)
      .redirectResolver(DefaultRedirectResolver())
      .tokenStore(tokenStore())
      .accessTokenConverter(tokenEnhancer())
  }

  @Bean
  open fun tokenEnhancer() = JwtAccessTokenConverter()
    .also { it.setSigningKey(authorizationServerProperties.privateKey) }
    .also { it.setVerifierKey(authorizationServerProperties.publicKey) }

  @Bean
  open fun tokenStore(): JwtTokenStore {
    return JwtTokenStore(tokenEnhancer())
  }

}