package io.tobbit.identity.config

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.web.authentication.NullRememberMeServices
import org.springframework.security.web.authentication.RememberMeServices
import org.springframework.social.UserIdSource
import org.springframework.social.connect.ConnectionFactoryLocator
import org.springframework.social.connect.ConnectionSignUp
import org.springframework.social.connect.UsersConnectionRepository
import org.springframework.social.connect.mem.InMemoryUsersConnectionRepository
import org.springframework.social.security.AuthenticationNameUserIdSource
import org.springframework.social.security.SocialAuthenticationServiceRegistry
import org.springframework.social.security.provider.SocialAuthenticationService


@Configuration
open class AuthConfig {

  @Bean
  open fun authenticationServiceRegistry(
    @Qualifier("socialAuthenticationServices") services: List<SocialAuthenticationService<*>> = emptyList()
  ) = SocialAuthenticationServiceRegistry().also { services.forEach(it::addAuthenticationService) }

  @Bean
  open fun usersConnectionRepository(cfl: ConnectionFactoryLocator, csu: ConnectionSignUp): UsersConnectionRepository =
    InMemoryUsersConnectionRepository(cfl).also { it.setConnectionSignUp(csu) }

  @Bean
  open fun userIdSource(): UserIdSource = AuthenticationNameUserIdSource()

  @Bean
  open fun rememberMeServices(): RememberMeServices = NullRememberMeServices()

}