package io.tobbit.identity.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("spring.social")
class SocialProperties {
  val google = SocialAuthProperties()
  val facebook = SocialAuthProperties()
  val twitter = SocialAuthProperties()
  val linkedIn = SocialAuthProperties()

  open class SocialAuthProperties {
    open var clientId: String? = null
    open var clientSecret: String? = null
    open var scopes: List<String>? = null
  }
}
