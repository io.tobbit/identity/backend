package io.tobbit.identity.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("spring.oauth2.server")
class AuthorizationServerProperties {
  lateinit var privateKey: String
  lateinit var publicKey: String
}
