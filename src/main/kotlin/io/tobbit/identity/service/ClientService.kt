package io.tobbit.identity.service

import io.tobbit.identity.model.api.ClientInfo
import io.tobbit.identity.model.security.AppClientDetails
import io.tobbit.identity.service.security.AppClientDetailsService
import org.springframework.stereotype.Service
import java.awt.Color
import java.awt.Font
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO

@Service
class ClientService(
  private val clientDetailsService: AppClientDetailsService
) {

  fun getClientInfo(clientId: String): ClientInfo {
    return clientDetailsService.loadClientByClientId(clientId)
      .let(::toClientInfo)
  }

  fun getClientImage(clientId: String): ByteArrayOutputStream {
    return generateClientImage(getClientInfo(clientId).clientId)
  }

  private fun generateClientImage(clientId: String): ByteArrayOutputStream {
    val bufferedImage = BufferedImage(480, 240, BufferedImage.TYPE_INT_ARGB)
    val g2d = bufferedImage.createGraphics()

    g2d.color = Color.WHITE
    g2d.fillRect(0, 0, 480, 240)

    g2d.color = Color.black
    g2d.font = Font("Arial", Font.BOLD, 48)
    g2d.drawString(clientId, 160, 150)

    g2d.dispose()

    val outputStream = ByteArrayOutputStream()
    ImageIO.write(bufferedImage, "png", outputStream)
    return outputStream
  }

  private fun toClientInfo(it: AppClientDetails) = ClientInfo(
    it.clientId, it.clientId, "/client/${it.clientId}/image"
  )

}
