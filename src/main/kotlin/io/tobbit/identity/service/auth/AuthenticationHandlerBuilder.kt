package io.tobbit.identity.service.auth

import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import org.springframework.security.web.DefaultRedirectStrategy
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler
import org.springframework.security.web.authentication.logout.LogoutHandler
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler
import org.springframework.social.security.SocialAuthenticationFailureHandler
import org.springframework.stereotype.Service
import org.springframework.web.util.UriComponentsBuilder
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Service
class AuthenticationHandlerBuilder {

  fun logoutHandler(): LogoutHandler = SecurityContextLogoutHandler()

  fun logoutSuccessHandler(targetUrl: String): LogoutSuccessHandler = SimpleUrlLogoutSuccessHandler()
    .also { it.setDefaultTargetUrl(targetUrl) }

  fun loginErrorHandler(errorUrl: String) =
    SocialAuthenticationFailureHandler(SimpleUrlAuthenticationFailureHandler(errorUrl)
      .also { handler ->
        handler.setRedirectStrategy(object : DefaultRedirectStrategy() {
          override fun sendRedirect(
            request: HttpServletRequest,
            response: HttpServletResponse,
            url: String
          ) {
            val targetUri = calculateRedirectUrl(request.contextPath, url)
            super.sendRedirect(request, response, keepClientIdForTargetUrl(targetUri, request))
          }
        })
      })

  fun loginEntryPoint(url: String): AuthenticationEntryPoint = object : LoginUrlAuthenticationEntryPoint(url) {
    override fun determineUrlToUseForThisRequest(
      req: HttpServletRequest,
      res: HttpServletResponse,
      e: AuthenticationException
    ): String {
      val targetUri = super.determineUrlToUseForThisRequest(req, res, e)
      return keepClientIdForTargetUrl(targetUri, req)
        .let { UriComponentsBuilder.fromUriString(it) }
        .queryParam("next", req.requestURI)
        .toUriString()
    }
  }

  fun loginSuccessHandler(): AuthenticationSuccessHandler = object : SimpleUrlAuthenticationSuccessHandler() {
    override fun determineTargetUrl(
      req: HttpServletRequest,
      res: HttpServletResponse,
      auth: Authentication
    ): String {
      return if (!req.parameterMap["next"].isNullOrEmpty()) {
        req.parameterMap.getValue("next").first()
      } else {
        super.determineTargetUrl(req, res, auth)
      }
    }
  }

  private fun keepClientIdForTargetUrl(targetUri: String, req: HttpServletRequest): String {
    var builder = UriComponentsBuilder.fromUriString(targetUri)
    if (req.parameterMap.containsKey("client_id")) {
      builder = builder.queryParam("client_id", req.parameterMap["client_id"]?.toList())
    }
    return builder.toUriString()
  }
}