package io.tobbit.identity.service.auth

import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.web.authentication.RememberMeServices
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.authentication.logout.LogoutFilter
import org.springframework.social.UserIdSource
import org.springframework.social.connect.UsersConnectionRepository
import org.springframework.social.security.SocialAuthenticationFilter
import org.springframework.social.security.SocialAuthenticationServiceLocator
import org.springframework.stereotype.Service
import javax.servlet.http.HttpServletRequest

@Service
class AuthenticationFilterBuilder(
  private val usersConnectionRepository: UsersConnectionRepository,
  private val userIdSource: UserIdSource,
  private val authServiceLocator: SocialAuthenticationServiceLocator,
  private val ahb: AuthenticationHandlerBuilder,
  private val rememberMeServices: RememberMeServices
) {

  fun socialLoginFilter(authenticationManager: AuthenticationManager, signInUrl: String, errorUrl: String) =
    object : SocialAuthenticationFilter(authenticationManager, userIdSource, usersConnectionRepository, authServiceLocator) {
      override fun detectRejection(request: HttpServletRequest): Boolean {
        val ignorableKeys = listOf("client_id", "next")
        val parameterKeys = request.parameterMap.keys.filter{ !ignorableKeys.contains(it) }
        return if (parameterKeys.size == 1 && parameterKeys.contains("state")) {
          false
        } else {
          parameterKeys.isNotEmpty()
            && !parameterKeys.contains("oauth_token")
            && !parameterKeys.contains("code")
            && !parameterKeys.contains("scope")
        }
      }
    }
      .also { it.setAuthenticationSuccessHandler(ahb.loginSuccessHandler()) }
      .also { it.setAuthenticationFailureHandler(ahb.loginErrorHandler(errorUrl)) }
      .also { it.setFilterProcessesUrl(signInUrl) }
      .also { it.rememberMeServices = rememberMeServices }

  fun formLoginFilter(authenticationManager: AuthenticationManager, formLoginUrl: String, errorUrl: String) =
    UsernamePasswordAuthenticationFilter(authenticationManager)
      .also { it.setAuthenticationSuccessHandler(ahb.loginSuccessHandler()) }
      .also { it.setAuthenticationFailureHandler(ahb.loginErrorHandler(errorUrl)) }
      .also { it.setFilterProcessesUrl(formLoginUrl) }
      .also { it.rememberMeServices = rememberMeServices }

  fun logoutFilter(targetUrl: String): LogoutFilter =
    LogoutFilter(ahb.logoutSuccessHandler(targetUrl), ahb.logoutHandler())
}