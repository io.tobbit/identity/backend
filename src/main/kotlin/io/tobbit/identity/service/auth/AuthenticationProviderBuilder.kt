package io.tobbit.identity.service.auth

import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.core.userdetails.UserDetailsPasswordService
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.social.connect.UsersConnectionRepository
import org.springframework.social.security.SocialAuthenticationProvider
import org.springframework.social.security.SocialUserDetailsService
import org.springframework.stereotype.Service

@Service
class AuthenticationProviderBuilder(
  private val usersConnectionRepository: UsersConnectionRepository,
  private val userDetailsService: UserDetailsService,
  private val userDetailsPasswordService: UserDetailsPasswordService,
  private val socialUserDetailsService: SocialUserDetailsService,
  private val passwordEncoder: PasswordEncoder
) {

  fun socialAuthenticationProvider() =
    SocialAuthenticationProvider(usersConnectionRepository, socialUserDetailsService)

  fun formLoginAuthenticationProvider() =
    DaoAuthenticationProvider()
      .also { it.setPasswordEncoder(passwordEncoder) }
      .also { it.setUserDetailsService(userDetailsService) }
      .also { it.setUserDetailsPasswordService(userDetailsPasswordService) }
}