package io.tobbit.identity.service.auth

import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.util.UriComponentsBuilder
import javax.servlet.http.HttpServletRequest

@Service
class AuthenticationSessionService {

  private val loginRedirectUriAttribute = "redirectAfterLogin"

  fun saveLoginRedirectUri(req: HttpServletRequest) {
    if (HttpMethod.GET.matches(req.method)) {
      val uri = UriComponentsBuilder.fromUriString(req.requestURI)
        .queryParams(LinkedMultiValueMap(req.parameterMap.mapValues { it.value.toList() }))
        .toUriString()
      req.session.setAttribute(loginRedirectUriAttribute, uri)
    }
  }

  fun getAndClearLoginRedirectUri(req: HttpServletRequest): String? {
    return getAndClearAttribute(req, loginRedirectUriAttribute)
  }

  private fun getAndClearAttribute(req: HttpServletRequest, attribute: String): String? {
    val session = req.session
    val attributeNames = session.attributeNames.toList()
    return if (attributeNames.contains(attribute)) {
      val targetUri = session.getAttribute(attribute) as String
      session.removeAttribute(attribute)
      targetUri
    } else {
      null
    }
  }

}