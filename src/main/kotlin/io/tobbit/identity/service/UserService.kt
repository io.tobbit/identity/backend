package io.tobbit.identity.service

import io.tobbit.identity.model.api.UserInfo
import io.tobbit.identity.model.api.UserRegistrationInfo
import io.tobbit.identity.model.security.AppUserDetails
import io.tobbit.identity.service.security.AppConnectionSignUp
import io.tobbit.identity.service.security.AppUserDetailsService
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class UserService(
  private val signUp: AppConnectionSignUp,
  private val userDetailsService: AppUserDetailsService
) {

  fun getUser(): UserInfo? {
    return SecurityContextHolder.getContext()
      .authentication.principal
      .takeIf { it is AppUserDetails }
      ?.let { it as AppUserDetails }
      ?.username
      ?.let { userDetailsService.loadUserByUsername(it) }
      ?.let(::toUserInfo)
  }

  fun registerUser(user: UserRegistrationInfo): UserInfo? {
    return try {
      userDetailsService.loadUserByUsername(user.userName)
      null
    } catch (e: UsernameNotFoundException) {
      signUp.registerUser(null, user.userName, user.password)
      userDetailsService.loadUserByUsername(user.userName)
        .let(::toUserInfo)
    }
  }

  private fun toUserInfo(user: AppUserDetails) = UserInfo(user.userId, user.username)

}
