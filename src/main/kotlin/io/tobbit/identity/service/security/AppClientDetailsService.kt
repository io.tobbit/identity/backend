package io.tobbit.identity.service.security

import io.tobbit.identity.model.exception.ClientNotFoundException
import io.tobbit.identity.model.security.AppClientDetails
import org.springframework.context.annotation.Primary
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.oauth2.provider.ClientDetailsService
import org.springframework.stereotype.Service

@Service
@Primary
class AppClientDetailsService(passwordEncoder: PasswordEncoder) : ClientDetailsService {

  private val clients = listOf(
    AppClientDetails(
      clientId = "client-1",
      clientSecret = passwordEncoder.encode("client-1-secret"),
      authorizationGrantTypes = mutableSetOf("authorization_code"),
      scope = mutableSetOf("user_info"),
      registeredRedirectUri = mutableSetOf("https://oauth.pstmn.io/v1/callback")
    )
  )

  override fun loadClientByClientId(clientId: String?): AppClientDetails {
    return clients
      .firstOrNull { it.clientId == clientId }
      ?: throw ClientNotFoundException(clientId)
  }
}