package io.tobbit.identity.service.security

import io.tobbit.identity.model.exception.UserNotFoundException
import io.tobbit.identity.model.jpa.AppUser
import io.tobbit.identity.model.security.AppUserDetails
import io.tobbit.identity.repository.jpa.AppUserRepository
import org.springframework.context.annotation.Primary
import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsPasswordService
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.social.security.SocialUserDetailsService
import org.springframework.stereotype.Service

@Service
@Primary
class AppUserDetailsService(
  private val repository: AppUserRepository,
  private val passwordEncoder: PasswordEncoder
) : UserDetailsService, SocialUserDetailsService, UserDetailsPasswordService {

  override fun loadUserByUsername(username: String): AppUserDetails {
    return repository.findByUserName(username)
      ?.let(::mapUser)
      ?: fail(username)
  }

  override fun loadUserByUserId(userId: String): AppUserDetails {
    return repository.findByIdOrNull(userId)
      ?.let(::mapUser)
      ?: fail(userId)
  }

  private fun mapUser(user: AppUser): AppUserDetails = AppUserDetails(
    user.userId, user.userName, user.encryptedPassword,
    user.roles.map(::SimpleGrantedAuthority),
    user.enabled,
    accountNonExpired = true,
    accountNonLocked = true,
    credentialsNonExpired = true
  )

  override fun updatePassword(userDetails: UserDetails, password: String): AppUserDetails =
    repository.findByUserName(userDetails.username)
      ?.also { it.encryptedPassword = passwordEncoder.encode(password) }
      ?.let(repository::save)
      ?.let(::mapUser)
      ?: fail(userDetails.username)

  private fun fail(username: String): AppUserDetails {
    throw UserNotFoundException(username)
  }
}
