package io.tobbit.identity.service.security

import io.tobbit.identity.model.jpa.AppUser
import io.tobbit.identity.model.api.UserRole
import io.tobbit.identity.repository.jpa.AppUserRepository
import org.springframework.data.repository.findByIdOrNull
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.social.connect.Connection
import org.springframework.social.connect.ConnectionKey
import org.springframework.social.connect.ConnectionSignUp
import org.springframework.stereotype.Service
import java.util.*

@Service
class AppConnectionSignUp(
  private val appUserRepository: AppUserRepository,
  private val passwordEncoder: PasswordEncoder
) : ConnectionSignUp {

  override fun execute(connection: Connection<*>): String {
    val connectionKey = connection.key
    val id = generateProviderId(connectionKey)
    val name = connection.displayName
    val user = appUserRepository.findByIdOrNull(id)
      ?: registerUser(id, name, UUID.randomUUID().toString().substring(0, 32))
    return user.userId
  }

  fun registerUser(id: String?, userName: String, password: String): AppUser {
    val userId = id ?: generateLocalId()
    return appUserRepository.save(
      AppUser()
        .also { it.userId = userId }
        .also { it.userName = userName }
        .also { it.encryptedPassword = passwordEncoder.encode(password) }
        .also { it.enabled = true }
        .also { it.roles = mutableSetOf(UserRole.ROLE_USER.name) }
    )
  }

  private fun generateLocalId(): String {
    var generatedId: String
    do {
      generatedId = ("custom:" + UUID.randomUUID().toString()).substring(0, 36)
    } while (appUserRepository.existsById(generatedId))
    return generatedId
  }

  private fun generateProviderId(connectionKey: ConnectionKey) =
    "${connectionKey.providerId}:${connectionKey.providerUserId}"

}
