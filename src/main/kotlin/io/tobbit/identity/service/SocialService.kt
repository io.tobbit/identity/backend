package io.tobbit.identity.service

import io.tobbit.identity.config.properties.SocialProperties
import io.tobbit.identity.model.api.SocialLoginConfig
import org.springframework.stereotype.Service

@Service
class SocialService(socialProperties: SocialProperties) {

  val configuredServices = mapOf(
    "facebook" to socialProperties.facebook,
    "google" to socialProperties.google,
    "twitter" to socialProperties.twitter,
    "linkedin" to socialProperties.linkedIn
  ).filter { it.value.clientId != null && it.value.clientSecret != null && !it.value.scopes.isNullOrEmpty() }

  val loginConfigs = configuredServices
    .map { SocialLoginConfig(it.key, it.key) }

}
