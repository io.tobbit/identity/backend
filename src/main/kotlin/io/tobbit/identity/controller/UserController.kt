package io.tobbit.identity.controller

import io.tobbit.identity.model.api.UserInfo
import io.tobbit.identity.model.api.UserRegistrationInfo
import io.tobbit.identity.service.UserService
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.view.RedirectView

@Controller
@RequestMapping("/user")
open class UserController(private val userService: UserService) {

  @PostMapping("/register")
  open fun register(
    @ModelAttribute("username") userName: String,
    @ModelAttribute("password") password: String
  ): RedirectView {
    val registeredUser = userService.registerUser(UserRegistrationInfo(userName, password))
    return if (registeredUser == null) {
      RedirectView("/signup?fail")
    } else {
      RedirectView("/signup?success")
    }
  }

  @GetMapping("/me")
  @ResponseBody
  open fun getUser(): UserInfo? {
    return userService.getUser()
  }

}
