package io.tobbit.identity.controller

import io.tobbit.identity.model.api.SocialLoginConfig
import io.tobbit.identity.service.SocialService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/social")
open class SocialController(
  private val socialService: SocialService
) {

  @GetMapping("/login-configs")
  open fun loginConfigurations(): List<SocialLoginConfig> {
    return socialService.loginConfigs
  }

}