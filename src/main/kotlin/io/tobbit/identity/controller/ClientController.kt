package io.tobbit.identity.controller

import io.tobbit.identity.model.api.ClientInfo
import io.tobbit.identity.service.ClientService
import org.springframework.http.ContentDisposition
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/client")
open class ClientController(
  private val clientService: ClientService
) {

  @GetMapping("/{clientId}")
  open fun getClientInfo(@PathVariable("clientId") clientId: String): ClientInfo {
    return clientService.getClientInfo(clientId)
  }

  @GetMapping("/{clientId}/image")
  open fun getClientImage(@PathVariable("clientId") clientId: String): HttpEntity<ByteArray> {
    val outputStream = clientService.getClientImage(clientId)

    val headers = HttpHeaders()
      .also { it.contentType = MediaType.IMAGE_PNG }
      .also { it.contentDisposition = ContentDisposition.attachment().filename("$clientId.png").build() }

    return HttpEntity(outputStream.toByteArray(), headers)
  }

}