package io.tobbit.identity

import org.springframework.boot.Banner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
open class AuthApplication

fun main(args: Array<String>) {
  runApplication<AuthApplication>(*args) {
    setBannerMode(Banner.Mode.OFF)
  }
}
