package io.tobbit.identity.repository.jpa

import io.tobbit.identity.model.jpa.AppUser
import org.springframework.data.jpa.repository.JpaRepository

interface AppUserRepository : JpaRepository<AppUser, String> {
  fun findByUserName(userName: String): AppUser?
}
