package io.tobbit.identity.model.security

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.provider.ClientDetails

data class AppClientDetails(
  private val clientId: String,
  private val clientSecret: String,
  private val authorizationGrantTypes: MutableSet<String>,
  private val scope: MutableSet<String>,
  private val registeredRedirectUri: MutableSet<String>,
  private val resourceIds: MutableSet<String> = mutableSetOf(),
  private val secretRequired: Boolean = true,
  private val scoped: Boolean = true,
  private val authorities: MutableCollection<GrantedAuthority> = mutableListOf(),
  private val accessTokenValiditySeconds: Int = 3600,
  private val refreshTokenValiditySeconds: Int = 3600,
  private val autoApproveScopes: Set<String> = emptySet(),
  private val additionalInformation: MutableMap<String, Any> = mutableMapOf()
) : ClientDetails {
  override fun getClientId() = clientId

  override fun getResourceIds() = resourceIds

  override fun isSecretRequired() = secretRequired

  @JsonIgnore
  override fun getClientSecret() = clientSecret

  override fun isScoped() = scoped

  override fun getScope() = scope

  override fun getAuthorizedGrantTypes() = authorizationGrantTypes

  override fun getRegisteredRedirectUri() = registeredRedirectUri

  override fun getAuthorities() = authorities

  override fun getAccessTokenValiditySeconds() = accessTokenValiditySeconds

  override fun getRefreshTokenValiditySeconds() = refreshTokenValiditySeconds

  @JsonIgnore
  override fun isAutoApprove(scope: String) = autoApproveScopes.contains(scope)

  override fun getAdditionalInformation() = additionalInformation
}