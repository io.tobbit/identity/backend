package io.tobbit.identity.model.security

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.security.core.GrantedAuthority
import org.springframework.social.security.SocialUserDetails

data class AppUserDetails(
  private val userId: String,
  private val userName: String,
  private val password: String,
  private val authorities: Collection<GrantedAuthority>,
  private val enabled: Boolean,
  private val accountNonExpired: Boolean,
  private val accountNonLocked: Boolean,
  private val credentialsNonExpired: Boolean
) : SocialUserDetails {
  override fun getUserId(): String = userId
  override fun getUsername(): String = userName

  @JsonIgnore
  override fun getPassword(): String = password
  override fun getAuthorities(): Collection<GrantedAuthority> = authorities
  override fun isEnabled(): Boolean = enabled
  override fun isAccountNonExpired(): Boolean = accountNonExpired
  override fun isAccountNonLocked(): Boolean = accountNonLocked
  override fun isCredentialsNonExpired(): Boolean = credentialsNonExpired
}