package io.tobbit.identity.model.jpa

import javax.persistence.*

@Entity
@Table(name = "APP_USER")
open class AppUser {

  @Id
  @Column(name = "USER_ID", length = 36, nullable = false)
  open lateinit var userId: String

  @Column(name = "USER_NAME", length = 36, nullable = false)
  open lateinit var userName: String

  @Column(name = "ENCRYPTED_PASSWORD", length = 128, nullable = false)
  open lateinit var encryptedPassword: String

  @Column(name = "ENABLED", length = 1, nullable = false)
  open var enabled: Boolean = false

  @ElementCollection(fetch = FetchType.EAGER)
  @CollectionTable(name = "APP_USER_ROLE", joinColumns = [JoinColumn(name = "USER_ID")])
  @Column(name = "USER_ROLE")
  open var roles: MutableSet<String> = mutableSetOf()
}
