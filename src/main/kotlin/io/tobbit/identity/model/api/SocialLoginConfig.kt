package io.tobbit.identity.model.api

data class SocialLoginConfig(
  val name: String,
  val providerId: String,
)
