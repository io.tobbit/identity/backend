package io.tobbit.identity.model.api

data class UserInfo(
  val userId: String,
  val userName: String
)
