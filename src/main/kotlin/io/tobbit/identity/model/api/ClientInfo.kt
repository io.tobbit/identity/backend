package io.tobbit.identity.model.api

data class ClientInfo(
  val clientId: String,
  val clintName: String,
  val imageUrl: String
)