package io.tobbit.identity.model.api

enum class UserRole {
  ROLE_USER,
  ROLE_ADMIN
}
