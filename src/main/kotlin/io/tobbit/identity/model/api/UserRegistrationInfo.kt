package io.tobbit.identity.model.api


data class UserRegistrationInfo(
  val userName: String,
  val password: String,
)
