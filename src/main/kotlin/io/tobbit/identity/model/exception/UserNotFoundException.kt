package io.tobbit.identity.model.exception

import org.springframework.http.HttpStatus
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class UserNotFoundException(userId: String?, throwable: Throwable? = null) :
  UsernameNotFoundException("User with user-id $userId not found.", throwable)
