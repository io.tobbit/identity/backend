package io.tobbit.identity.model.exception

import org.springframework.http.HttpStatus
import org.springframework.security.oauth2.provider.ClientRegistrationException
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class ClientNotFoundException(clientId: String?, throwable: Throwable? = null) :
  ClientRegistrationException("Client with client-id $clientId not found.", throwable)
